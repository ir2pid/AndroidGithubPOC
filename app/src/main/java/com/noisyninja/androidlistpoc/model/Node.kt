package com.noisyninja.androidlistpoc.model


data class Node(
        val oid: String,
        val messageHeadline: String?=null,
        val pushedDate: String?=null
) : BaseDTO()