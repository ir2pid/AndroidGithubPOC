package com.noisyninja.androidlistpoc.model

import android.arch.persistence.room.TypeConverter
import android.databinding.BindingAdapter
import android.widget.ImageView
import com.google.gson.Gson
import com.noisyninja.androidlistpoc.R
import com.squareup.picasso.Picasso

/**
 * data binding converters
 * Created by sudiptadutta on 12/05/18.
 */

object DataConverter : BaseDTO() {

    @BindingAdapter("thumbnail")
    @JvmStatic
    fun loadImage(view: ImageView, imageUri: String?) {
        imageUri?.let {
            Picasso.with(view.context).load(imageUri)
                    .placeholder(R.drawable.ic_download)
                    .error(R.drawable.ic_error)
                    .fit().centerCrop()
                    .into(view)
        }
    }

    @TypeConverter
    @JvmStatic
    fun fromOwner(owner: Owner?): String? {
        return owner?.toString()
    }

    @TypeConverter
    @JvmStatic
    fun toOwner(ownerString: String?): Owner? {
        return ownerString.let {
            Gson().fromJson(ownerString, Owner::class.java)
        }
    }
}
