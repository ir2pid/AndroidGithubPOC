package com.noisyninja.androidlistpoc.model


data class Edge(
    val node: Node
)