package com.noisyninja.androidlistpoc.model


data class Repository(
    val id: Int,
    val ref: Ref
)