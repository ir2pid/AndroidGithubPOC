package com.noisyninja.androidlistpoc.model


data class Target(
    val history: History
)