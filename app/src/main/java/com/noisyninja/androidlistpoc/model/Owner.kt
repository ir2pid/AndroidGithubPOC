package com.noisyninja.androidlistpoc.model


class Owner(
        var avatar_url: String,
        var login: String
) : BaseDTO()