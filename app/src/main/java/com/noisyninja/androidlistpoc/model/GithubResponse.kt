package com.noisyninja.androidlistpoc.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverters


@Entity(tableName = "github")
data class GithubResponse(
        @field:PrimaryKey
        var id: Int? = 0,
        var name: String? = null,
        var private: Boolean? = false,
        var description: String? = null,
        var created_at: String? = null,
        var size: Int? = 0,
        var html_url: String? = null,
        var stargazers_count: Int? = 0,
        var watchers_count: Int? = 0,
        var language: String? = null,
        @field:TypeConverters(DataConverter::class)
        var owner: Owner? = null,
        var messageHeadline: String? = null,
        var pushedDate: String? = null
) : BaseDTO() {

    fun isCommitSynced(): Boolean {
        return messageHeadline != null
    }
}