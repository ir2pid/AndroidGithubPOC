package com.noisyninja.androidlistpoc.model


data class History(
    val edges: List<Edge>
)