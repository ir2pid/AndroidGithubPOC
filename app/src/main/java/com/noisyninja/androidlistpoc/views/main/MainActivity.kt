package com.noisyninja.androidlistpoc.views.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearLayoutManager.VERTICAL
import android.view.View.GONE
import android.view.View.VISIBLE
import com.noisyninja.androidlistpoc.NinjaApp
import com.noisyninja.androidlistpoc.R
import com.noisyninja.androidlistpoc.model.GithubResponse
import com.noisyninja.androidlistpoc.views.custom.MainAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*


open class MainActivity : AppCompatActivity(), IMainActivity {

    private var mResultList: ArrayList<GithubResponse> = ArrayList()
    lateinit var mIMainPresenter: IMainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        mIMainPresenter = MainPresenter(this, application as NinjaApp)
        setupList()
    }

    /**
     * setup UI widgets
     */
    private fun setupList() {
        val mLayoutManager = LinearLayoutManager(this)
        recyclerList.layoutManager = mLayoutManager
        recyclerList.adapter = MainAdapter(mResultList, mIMainPresenter)
        recyclerList.addItemDecoration(DividerItemDecoration(this, VERTICAL))
        refresh_layout.setOnRefreshListener {
            mIMainPresenter.getList()
            refresh_layout.isRefreshing = false
        }
    }

    /**
     * sets the list items once data is fetched from network/database
     */
    override fun setList(result: List<GithubResponse>) {
        mResultList.clear()
        mResultList.addAll(result)
        handleShowError(mResultList.isEmpty())
    }

    /**
     * show an error message if loading fails
     */
    private fun handleShowError(isError: Boolean) {
        recyclerList.adapter.notifyDataSetChanged()
        if (isError) {
            recyclerList.visibility = GONE
            progressBar.visibility = GONE
            recyclerContainer.visibility = VISIBLE
            recyclerText.text = getString(R.string.error_net)
        } else {
            recyclerList.visibility = VISIBLE
            recyclerContainer.visibility = GONE
        }
    }
}
