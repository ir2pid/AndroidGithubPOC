package com.noisyninja.androidlistpoc.views.custom

import android.databinding.DataBindingUtil
import android.support.annotation.NonNull
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.noisyninja.androidlistpoc.R
import com.noisyninja.androidlistpoc.databinding.ListMainBinding
import com.noisyninja.androidlistpoc.model.GithubResponse
import com.noisyninja.androidlistpoc.views.main.IMainPresenter


/**
 * main adapter for repository layout
 * Created by sudiptadutta on 12/05/18.
 */

class MainAdapter(private val mResultsList: ArrayList<GithubResponse>, private val mIMainPresenter: IMainPresenter) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent?.context)
        val binding = DataBindingUtil.inflate<ListMainBinding>(inflater, R.layout.list_main, parent, false)
        return GitViewHolder(binding.root, parent, binding)
    }

    override fun getItemCount(): Int {
        return mResultsList.size
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        return (viewHolder as GitViewHolder).bind(mResultsList[position], mIMainPresenter)

    }

    class GitViewHolder(itemView: View, val parent: ViewGroup?, private val binding: ListMainBinding) : RecyclerView.ViewHolder(itemView) {
        fun bind(@NonNull githubResponse: GithubResponse, @NonNull mainPresenter: IMainPresenter) {
            binding.github = githubResponse
            binding.mainPresenter = mainPresenter
            binding.executePendingBindings()
        }
    }
}
