package com.noisyninja.androidlistpoc.views.main

import com.noisyninja.androidlistpoc.model.GithubResponse

/**
 * presenter interface
 * Created by sudiptadutta on 12/05/18.
 */
interface IMainPresenter {
    fun showDetail(githubResponse: GithubResponse)
    fun getList()
}