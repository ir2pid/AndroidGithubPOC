package com.noisyninja.androidlistpoc.views.main

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.app.AppCompatActivity
import com.noisyninja.androidlistpoc.NinjaApp
import com.noisyninja.androidlistpoc.layers.UtilModule
import com.noisyninja.androidlistpoc.layers.database.viewmodel.GithubViewModel
import com.noisyninja.androidlistpoc.layers.database.viewmodel.ViewModelFactory
import com.noisyninja.androidlistpoc.model.GithubResponse
import javax.inject.Inject


/**
 * main presenter
 * Created by sudiptadutta on 12/05/18.
 */

class MainPresenter internal constructor(private val iMainActivity: IMainActivity, ninjaApp: NinjaApp) : IMainPresenter {

    @Inject
    lateinit var vmf: ViewModelFactory
    @Inject
    lateinit var util: UtilModule

    private var githubViewModel: GithubViewModel

    init {
        ninjaApp.ninjaComponent.injectMain(this)

        githubViewModel = ViewModelProviders.of(iMainActivity as AppCompatActivity, vmf).get(GithubViewModel::class.java)

        githubViewModel.githubLiveData.observe(iMainActivity as AppCompatActivity,
                Observer<List<GithubResponse>> { result -> handleResponse(result) })
    }

    /**
     * fetches the list of users from viewmodel which also acts as the database/network repository
     */
    override fun getList() {
        githubViewModel.getRepoList()
    }

    /**
     * opens repo detail in browser
     */
    override fun showDetail(githubResponse: GithubResponse) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(githubResponse.html_url))
        startActivity(iMainActivity as AppCompatActivity,browserIntent, null)
    }

    /**
     * handle response
     */
    private fun handleResponse(result: List<GithubResponse>?) {
        if (result == null) {
            util.logI("null response")
            iMainActivity.setList(ArrayList())
        } else {
            util.logI("got response")
            iMainActivity.setList(result)
        }
    }
}

