package com.noisyninja.androidlistpoc.views.main

import com.noisyninja.androidlistpoc.model.GithubResponse

/**
 * activity interface
 * Created by sudiptadutta on 12/05/18.
 */
interface IMainActivity {

    fun setList(result: List<GithubResponse>)

}
