package com.noisyninja.androidlistpoc.layers.network;

import com.noisyninja.androidlistpoc.BuildConfig;
import com.noisyninja.androidlistpoc.model.CommitResponse;
import com.noisyninja.androidlistpoc.model.GithubResponse;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * retrofit interface for db calls
 * Created by sudiptadutta on 06/02/18.
 */

public interface INetworkDao {

    @GET(BuildConfig.REPOS_URI)
    Observable<List<GithubResponse>> getRepos(@Header("Authorization") String token);

    @POST(BuildConfig.COMMITS_URI)
    Observable<CommitResponse> getCommits(@Header("Authorization") String token, @Body GitQL query);

}
