package com.noisyninja.androidlistpoc.layers.database.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.res.Resources
import com.noisyninja.androidlistpoc.R
import com.noisyninja.androidlistpoc.layers.UtilModule
import com.noisyninja.androidlistpoc.layers.database.DataBaseModule
import com.noisyninja.androidlistpoc.layers.network.NetworkModule
import javax.inject.Inject

/**
 * View model factory to provide viewmodels for different types
 * Created by sudiptadutta on 10/05/18.
 */

class ViewModelFactory @Inject constructor(private val dataBaseModule: DataBaseModule,
                                                private val utilModule: UtilModule,
                                                val resources: Resources,
                                                private val networkModule: NetworkModule) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(GithubViewModel::class.java)) {
            return GithubViewModel(dataBaseModule,
                    utilModule,
                    resources,
                    networkModule) as T
        }

        throw  IllegalArgumentException(resources.getString(R.string.unknownVMClass))
    }
}
