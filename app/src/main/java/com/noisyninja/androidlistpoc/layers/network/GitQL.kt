package com.noisyninja.androidlistpoc.layers.network

/**
 * class to hold graph queries for github
 * {@link https://developer.github.com/v4/guides/forming-calls/}
 */
data class GitQL(val query: String? = null)
