package com.noisyninja.androidlistpoc.layers.network;

import dagger.Module;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.noisyninja.androidlistpoc.BuildConfig.BASE_URL;

/**
 * Http client
 * Created by sudiptadutta on 27/04/18.
 */

@Module
public class HttpClient {

    private static Retrofit retrofit = null;

    public Retrofit getClient() {

        if (retrofit == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    //.cache(new Cache(mContext.getCacheDir(), Long.parseLong(CACHE_SIZE))) //enable for
                    .build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync()) //async to queue rxJava chain calls
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }
        return retrofit;
    }

}
