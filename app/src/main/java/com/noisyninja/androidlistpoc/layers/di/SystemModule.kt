package com.noisyninja.androidlistpoc.layers.di

import android.content.Context
import android.content.res.Resources
import com.noisyninja.androidlistpoc.NinjaApp
import com.noisyninja.androidlistpoc.layers.UtilModule
import dagger.Module
import dagger.Provides

/**
 * module containing System modules
 * Created by sudiptadutta on 27/04/18.
 */

@Module
class SystemModule(private val application: NinjaApp) {

    @Provides
    fun provideApplication(): NinjaApp {
        return application
    }

    @Provides
    fun provideApplicationContext(): Context {
        return application.applicationContext
    }

    @Provides
    fun provideResources(): Resources {
        return application.resources
    }

    @Provides
    fun provideUtil(context: Context): UtilModule {
        return UtilModule(context)
    }
}