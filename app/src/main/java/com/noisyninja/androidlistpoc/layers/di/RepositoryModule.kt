package com.noisyninja.androidlistpoc.layers.di

import android.content.res.Resources
import com.noisyninja.androidlistpoc.NinjaApp
import com.noisyninja.androidlistpoc.layers.UtilModule
import com.noisyninja.androidlistpoc.layers.database.DataBaseModule
import com.noisyninja.androidlistpoc.layers.database.viewmodel.ViewModelFactory
import com.noisyninja.androidlistpoc.layers.network.HttpClient
import com.noisyninja.androidlistpoc.layers.network.NetworkModule
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

/**
 * module containing Repository modules for storage and network
 * @see NetworkModule
 * @see DataBaseModule
 *
 * Created by sudiptadutta on 27/04/18.
 */

@Module
class RepositoryModule {

    @Provides
    fun provideViewModelFactory(dataBaseModule: DataBaseModule,
                                utilModule: UtilModule,
                                resources: Resources,
                                networkModule: NetworkModule): ViewModelFactory {
        return ViewModelFactory(dataBaseModule, utilModule, resources, networkModule)
    }

    @Provides
    fun provideRetrofit(): Retrofit {
        return HttpClient().client
    }

    @Provides
    fun provideNetwork(retrofit: Retrofit): NetworkModule {
        return NetworkModule(retrofit)
    }

    @Provides
    fun provideDataBase(utilModule: UtilModule, application: NinjaApp): DataBaseModule {
        return DataBaseModule(utilModule, application)
    }

}