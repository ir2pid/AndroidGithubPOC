package com.noisyninja.androidlistpoc.layers

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.google.gson.GsonBuilder
import timber.log.Timber
import javax.inject.Inject

/**
 * Module to hold all utility methods
 * Created by sudiptadutta on 27/04/18.
 */
open class UtilModule @Inject
constructor(private val mContext: Context) {

    fun getStringRes(@StringRes resId: Int, attr: String): String {
        return mContext.getString(resId, attr)
    }

    fun getStringRes(@StringRes resId: Int): String {
        return mContext.getString(resId)
    }

    fun getStringPref(key: String): String? {
        return mContext.getSharedPreferences(DEFAULT_PREF, MODE_PRIVATE).getString(key, null)
    }

    fun setStringPref(key: String, value: String) {
        val editor = mContext.getSharedPreferences(DEFAULT_PREF, MODE_PRIVATE).edit()
        editor.putString(key, value)
        editor.apply()
    }

    fun deleteStringPref(key: String) {
        mContext.getSharedPreferences(DEFAULT_PREF, MODE_PRIVATE).edit().remove(key).apply()
    }

    fun showSnackBar(view: View, text: String) {
        val snackbar = Snackbar.make(view, text, Snackbar.LENGTH_SHORT)
        val contentLay = snackbar.view.findViewById<View>(android.support.design.R.id.snackbar_text).parent as ViewGroup
        val item = ProgressBar(view.context)
        contentLay.addView(item)
        snackbar.show()
        //.setAction("Action", null).show();
    }

    fun logI(text: String) {
        Timber.i(text)
    }

    fun toJson(`object`: Any): String {
        return GsonBuilder().setPrettyPrinting().create().toJson(`object`)
    }

    fun <T> fromJson(string: String, t: Class<T>): T {
        return GsonBuilder().setPrettyPrinting().create().fromJson(string, t)
    }

    companion object {
        private const val DEFAULT_PREF = "DEFAULT_PREF"
    }

}