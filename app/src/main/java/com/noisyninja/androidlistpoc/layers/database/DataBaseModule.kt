package com.noisyninja.androidlistpoc.layers.database

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Room
import android.content.Context
import com.noisyninja.androidlistpoc.BuildConfig
import com.noisyninja.androidlistpoc.layers.UtilModule
import com.noisyninja.androidlistpoc.model.GithubResponse
import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


/**
 * database client to implement DatabaseDao @see DatabaseDao
 *
 * Created by sudiptadutta on 30/04/18.
 */
open class DataBaseModule @Inject constructor(internal var mUtilModule: UtilModule, context: Context) : DatabaseDao {

    private var mDataBase: IDatabase = Room.databaseBuilder(context,
            IDatabase::class.java, BuildConfig.APP_DB)
            //.allowMainThreadQueries() //do not use!!
            .build()

    override fun getAllRepos(): LiveData<List<GithubResponse>> {
        return mDataBase.databaseDao().allRepos
    }

    override fun findRepoById(id: Int): LiveData<GithubResponse> {
        return mDataBase.databaseDao().findRepoById(id)
    }

    override fun insertRepo(githubResponse: GithubResponse?) {

        Completable.fromAction {
            mUtilModule.logI("inserting")
            mDataBase.databaseDao().insertRepo(githubResponse)
        }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {
                        mUtilModule.logI("inserting done")
                    }

                    override fun onError(e: Throwable) {
                        mUtilModule.logI("inserting error " + e.message)
                    }
                })
    }

    override fun deleteRepo(githubResponse: GithubResponse?) {

        Completable.fromAction {
            mUtilModule.logI("deleting")
            mDataBase.databaseDao().deleteRepo(githubResponse)
        }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {
                        mUtilModule.logI("delete done")
                    }

                    override fun onError(e: Throwable) {
                        mUtilModule.logI("delete error " + e.message)
                    }
                })
    }

    override fun insertRepoList(githubResponseList: List<GithubResponse>?) {
        Completable.fromAction {
            mUtilModule.logI("inserting")
            mDataBase.databaseDao().insertRepoList(githubResponseList)
        }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {
                        mUtilModule.logI("insert all done")
                    }

                    override fun onError(e: Throwable) {
                        mUtilModule.logI("insert all error " + e.message)
                    }
                })
    }
}