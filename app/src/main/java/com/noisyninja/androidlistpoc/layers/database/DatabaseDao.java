package com.noisyninja.androidlistpoc.layers.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.noisyninja.androidlistpoc.model.GithubResponse;

import java.util.List;

/**
 * Database data access object
 * Created by sudiptadutta on 30/04/18.
 */

@Dao
public interface DatabaseDao {

    @Query("SELECT * FROM github")
    LiveData<List<GithubResponse>> getAllRepos();

    @Query("SELECT * FROM github where id LIKE  :id")
    LiveData<GithubResponse> findRepoById(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertRepo(GithubResponse githubResponse);

    @Delete
    void deleteRepo(GithubResponse githubResponse);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertRepoList(List<GithubResponse> githubResponseList);

}