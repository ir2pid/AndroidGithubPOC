package com.noisyninja.androidlistpoc.layers.di

import android.content.Context
import android.content.res.Resources
import com.noisyninja.androidlistpoc.NinjaApp
import com.noisyninja.androidlistpoc.layers.UtilModule
import com.noisyninja.androidlistpoc.layers.database.DataBaseModule
import com.noisyninja.androidlistpoc.layers.database.viewmodel.ViewModelFactory
import com.noisyninja.androidlistpoc.layers.network.NetworkModule
import com.noisyninja.androidlistpoc.views.main.MainPresenter
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * module interface contains modules:
 * @see SystemModule
 * @see RepositoryModule
 * Created by sudiptadutta on 27/04/18.
 */

@Singleton
@Component(modules = [(AndroidSupportInjectionModule::class), (SystemModule::class), (RepositoryModule::class)])
interface NinjaComponent {
    fun injectMain(mainPresenter: MainPresenter)
    fun app(): NinjaApp
    fun appContext(): Context
    fun resources(): Resources
    fun network(): NetworkModule
    fun vmf(): ViewModelFactory
    fun database(): DataBaseModule
    fun util(): UtilModule
}