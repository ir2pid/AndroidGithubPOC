package com.noisyninja.androidlistpoc.layers.network

import com.noisyninja.androidlistpoc.BuildConfig.AUTH_KEY
import com.noisyninja.androidlistpoc.model.CommitResponse
import com.noisyninja.androidlistpoc.model.GithubResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import javax.inject.Inject

/**
 * network module to make network calls
 * Created by sudiptadutta on 27/04/18.
 */

open class NetworkModule @Inject constructor(private val retrofit: Retrofit) {

    fun getRepos(): Observable<List<GithubResponse>> {
        return retrofit.create(INetworkDao::class.java)
                .getRepos(AUTH_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun getCommits(graphQL: GitQL): Observable<CommitResponse> {
        return retrofit.create(INetworkDao::class.java)
                .getCommits(AUTH_KEY, graphQL)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}
