package com.noisyninja.androidlistpoc.layers.database.viewmodel

import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.ViewModel
import android.content.res.Resources
import com.noisyninja.androidlistpoc.R
import com.noisyninja.androidlistpoc.layers.UtilModule
import com.noisyninja.androidlistpoc.layers.database.DataBaseModule
import com.noisyninja.androidlistpoc.layers.network.GitQL
import com.noisyninja.androidlistpoc.layers.network.NetworkModule
import com.noisyninja.androidlistpoc.model.GithubResponse
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


/**
 * view model for GithubResponse, also acts as the repository which syncs with network
 * and fetches from database when a connection isn't available
 * Created by sudiptadutta on 10/05/18.
 */
open class GithubViewModel @Inject constructor(private val dataBaseModule: DataBaseModule,
                                               private val utilModule: UtilModule,
                                               val resources: Resources,
                                               private val networkModule: NetworkModule) : ViewModel() {

    private var compositeDisposables = CompositeDisposable()
    val githubLiveData: MediatorLiveData<List<GithubResponse>> = MediatorLiveData()

    init {
        val dbSource = dataBaseModule.allRepos
        utilModule.logI("syncing database")
        githubLiveData.addSource(dbSource) { t ->
            githubLiveData.value = t
        }
        getRepoList()
    }

    fun getRepoList() {
        utilModule.logI("syncing net")
        cleanDisposables()
        compositeDisposables
                .add(networkModule
                        .getRepos()
                        .flatMap { itemList ->
                            //1.fetches all repos and saves
                            dataBaseModule.insertRepoList(itemList)                 //add all repo list from first call
                            Observable.fromIterable(itemList)
                        }
                        .concatMapEager { githubItem ->
                            //2.fetches commits of each repos and saves to database
                            val glQL = GitQL(utilModule.getStringRes(R.string.query, githubItem.name!!))
                            networkModule.getCommits(glQL).map { commitItem ->
                                val node = commitItem.data.repository.ref.target.history.edges[0].node
                                githubItem.messageHeadline = node.messageHeadline ?: utilModule.getStringRes(R.string.error_data)
                                githubItem.pushedDate = node.pushedDate ?: utilModule.getStringRes(R.string.error_data)
                                dataBaseModule.insertRepo(githubItem)
                            }.doOnError {
                                //3.on commit fetch error saves error message to database
                                githubItem.messageHeadline = utilModule.getStringRes(R.string.error_data)
                                githubItem.pushedDate = utilModule.getStringRes(R.string.error_data)
                                dataBaseModule.insertRepo(githubItem)
                            }.onErrorResumeNext(Observable.empty())
                        }.doOnComplete { cleanDisposables() }
                        .subscribe(
                                {
                                    //onNext
                                    utilModule.logI("net repo next")

                                },
                                {
                                    //onError
                                    utilModule.logI("net repo error")
                                    //cleanDisposables()
                                },
                                {
                                    //onComplete
                                    utilModule.logI("net repo complete")
                                    //cleanDisposables()
                                }
                        ))
    }

    private fun cleanDisposables() {
        //clean old network observables, dispose will prevent further observing
        utilModule.logI("disposing observables")
        if (!compositeDisposables.isDisposed) {
            compositeDisposables.clear()
        }
    }

    override fun onCleared() {
        cleanDisposables()
        super.onCleared()
    }
}
