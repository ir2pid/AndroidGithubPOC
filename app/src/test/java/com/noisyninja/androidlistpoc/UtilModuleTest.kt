package com.noisyninja.androidlistpoc

import com.noisyninja.androidlistpoc.model.GithubResponse
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class UtilModuleTest : BaseUnit() {

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        setUpMocks()
        setupReturns()
    }

    /**
     * 1.test getString returns mock_string,
     * 2.check context.getString is called
     */
    @Test
    fun getStringResTest() {
        Assert.assertEquals(utilModule.getStringRes(R.string.app_name), mock_string)
        verify(context).getString(R.string.app_name)
    }

    /**
     * check for json marshalling
     */
    @Test
    fun toJsonTest() {
        val json = utilModule.toJson(githubResponse)
        Assert.assertNotNull(json)
    }

    /**
     * check for json un-marshalling
     */
    @Test
    fun fromJsonTest() {
        val json = utilModule.toJson(githubResponse)
        Assert.assertEquals(githubResponse.name, utilModule.fromJson(json, GithubResponse::class.java).name)
    }
}
