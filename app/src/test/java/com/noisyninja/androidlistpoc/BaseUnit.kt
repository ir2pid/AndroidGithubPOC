package com.noisyninja.androidlistpoc

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.noisyninja.androidlistpoc.layers.UtilModule
import com.noisyninja.androidlistpoc.layers.network.HttpClient
import com.noisyninja.androidlistpoc.layers.network.NetworkModule
import com.noisyninja.androidlistpoc.model.GithubResponse
import com.noisyninja.androidlistpoc.model.Owner
import org.mockito.Mock
import org.mockito.Mockito

/**
 * Created by sudiptadutta on 23/05/18.
 */
open class BaseUnit {

    @Mock
    lateinit var context: Context
    @Mock
    lateinit var connectivityManager: ConnectivityManager
    @Mock
    lateinit var networkInfo: NetworkInfo
    @Mock
    lateinit var httpClient: HttpClient

    lateinit var utilModule: UtilModule
    lateinit var networkModule: NetworkModule
    lateinit var githubResponse: GithubResponse

    val page = 1

    fun setUpMocks() {
        utilModule = UtilModule(context)
        githubResponse = GithubResponse(100,"testname",
                false,"testdescription",
                "10/05/2018",1,"testURL",
        10,5, "swift",
                Owner("testURL","me"),"testheadline",
                "10/5/2018"
        )
    }

    fun setupReturns() {
        Mockito.`when`(context.getString(R.string.app_name)).thenReturn(mock_string)
        Mockito.`when`(context.getSystemService(Context.CONNECTIVITY_SERVICE)).thenReturn(connectivityManager)
        Mockito.`when`(connectivityManager.activeNetworkInfo).thenReturn(networkInfo)
        // Mockito.`when`(httpClient.client).thenReturn(Retrofit.Builder().baseUrl(BuildConfig.BASE_URL).build())
        // Mockito.`when`(networkModule.getPeople(networkCallback, page, BuildConfig.RESULT_COUNT.toInt(), BuildConfig.NETSYNC_SEED_VALUE.toInt())).thenReturn(GenericObserver(networkCallback))
    }

    companion object {
        val mock_string = "mock_string"
    }

}