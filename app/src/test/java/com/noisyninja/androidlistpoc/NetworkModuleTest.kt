package com.noisyninja.androidlistpoc

import com.noisyninja.androidlistpoc.layers.network.NetworkModule
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.MockitoAnnotations
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class NetworkModuleTest : BaseRepository() {

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        setUpMocks()
        //setupReturns()
        setUpRepository()
        setupLoopers()
    }

    @After
    @Throws(IOException::class)
    fun tearDown() {
        tearDownRepository()
        tearDownLoopers()
    }

    @Test
    fun serverCallWithSuccess() {
        //Given
        val url = BuildConfig.BASE_URL

        val interceptor = HttpLoggingInterceptor()
                .setLevel(HttpLoggingInterceptor.Level.BODY)

        val client = OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build()

        val retrofit = Retrofit.Builder()
                .baseUrl(mMockWebServer.url(url))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()

        val remoteDataSource = NetworkModule(retrofit)

        val vehicleResponse = remoteDataSource
                .getRepos()

        vehicleResponse.subscribe(mSubscriber)
        mSubscriber.assertNoErrors()
        mSubscriber.assertValue({ it ->
            it.isNotEmpty()
        })
        mSubscriber.assertValue({ it ->
            it[0].name?.isNotEmpty()!! &&
                    it[0].id != 0 &&
                    it[0].html_url?.isNotEmpty()!!
        })
    }

    /**
     * test for network availability
     */
    @Test
    fun serverCallWithError() {
        //this is an invalid uri
        val url = BuildConfig.BASE_URL + "/PathDoesNotExist/"

        val interceptor = HttpLoggingInterceptor()
                .setLevel(HttpLoggingInterceptor.Level.BODY)

        val client = OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build()

        val retrofit = Retrofit.Builder()
                .baseUrl(mMockWebServer.url(url))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()

        val remoteDataSource = NetworkModule(retrofit)

        val vehicleResponse = remoteDataSource
                .getRepos()

        vehicleResponse.subscribe(mSubscriber)
        mSubscriber.assertError({ t: Throwable -> t.message!!.isNotEmpty() })

    }

}