package com.noisyninja.androidlistpoc

import android.content.Context
import android.content.res.Resources
import android.support.test.InstrumentationRegistry
import com.noisyninja.androidlistpoc.layers.UtilModule
import com.noisyninja.androidlistpoc.layers.database.DataBaseModule
import com.noisyninja.androidlistpoc.layers.database.viewmodel.ViewModelFactory
import com.noisyninja.androidlistpoc.layers.di.NinjaComponent
import com.noisyninja.androidlistpoc.layers.network.HttpClient
import com.noisyninja.androidlistpoc.layers.network.NetworkModule

/**
 * Mock test application with mocked dagger dependencies
 * Created by sudiptadutta on 20/05/18.
 */
class TestApplication : NinjaApp() {

    private lateinit var appContext: Context
    private lateinit var testApplication: TestApplication
    private lateinit var resourcesModule: Resources
    private lateinit var utilModule: UtilModule
    private lateinit var networkModule: NetworkModule
    private lateinit var dataBaseModule: DataBaseModule
    private lateinit var viewModelFactory: ViewModelFactory

    override val ninjaComponent: NinjaComponent by lazy {
        initialise()

        DaggerTestComponent.builder()
                .database(dataBaseModule)
                .app(testApplication)
                .appContext(appContext)
                .network(networkModule)
                .resources(resourcesModule)
                .util(utilModule)
                .vmf(viewModelFactory)
                .build()
    }

    private fun initialise() {
        testApplication = InstrumentationRegistry.getTargetContext().applicationContext as TestApplication
        appContext = InstrumentationRegistry.getTargetContext()
        utilModule = UtilModule(appContext)
        resourcesModule = appContext.resources
        networkModule = NetworkModule(HttpClient().client)
        dataBaseModule = DataBaseModule(utilModule, appContext)
        viewModelFactory = ViewModelFactory(dataBaseModule, utilModule, resources, networkModule)
    }
}