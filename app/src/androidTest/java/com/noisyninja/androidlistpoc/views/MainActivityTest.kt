package com.noisyninja.androidlistpoc.views

import android.content.Context
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.IdlingRegistry
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.noisyninja.androidlistpoc.MainActivityIdlingResource
import com.noisyninja.androidlistpoc.R
import com.noisyninja.androidlistpoc.TestApplication
import com.noisyninja.androidlistpoc.views.main.MainActivity
import com.noisyninja.androidlistpoc.views.main.MainPresenter
import junit.framework.Assert
import kotlinx.android.synthetic.main.content_main.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by sudiptadutta on 18/05/18.
 */

@RunWith(AndroidJUnit4::class)
class MainActivityTest : BaseTest() {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule<MainActivity>(MainActivity::class.java)
    lateinit var context: Context
    lateinit var app: TestApplication
    private lateinit var mainActivity: MainActivity
    private lateinit var mainPresenter: MainPresenter
    private lateinit var idlingResource: MainActivityIdlingResource

    @Before
    fun setup() {
        context = InstrumentationRegistry.getTargetContext()
        app = InstrumentationRegistry.getTargetContext().applicationContext as TestApplication
        mainActivity = mActivityTestRule.activity
        mainPresenter = mainActivity.mIMainPresenter as MainPresenter

        idlingResource = MainActivityIdlingResource(mActivityTestRule.activity.recyclerList, mActivityTestRule.activity.javaClass.simpleName)
        IdlingRegistry.getInstance().register(idlingResource)
    }

    @After
    fun teardown() {
        IdlingRegistry.getInstance().unregister(idlingResource)
    }

    /**
     * test if adapter has items fetched
     */
    @Test
    fun checkListCountTest() {
        sleepMedium()
        onView(withId(R.id.recyclerList)).check(matches(isDisplayed()))
        Assert.assertTrue(mainActivity.recyclerList.adapter.itemCount > 0)
    }

}
