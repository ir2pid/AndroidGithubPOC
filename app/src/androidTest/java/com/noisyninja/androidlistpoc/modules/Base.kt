package com.noisyninja.androidlistpoc.modules

import android.content.Context
import android.support.test.InstrumentationRegistry
import com.noisyninja.androidlistpoc.TestApplication
import com.noisyninja.androidlistpoc.layers.UtilModule
import com.noisyninja.androidlistpoc.model.GithubResponse
import com.noisyninja.androidlistpoc.model.Owner
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers


open class Base {

    lateinit var mContext: Context
    lateinit var mApp: TestApplication
    lateinit var mUtilModule: UtilModule

    val githubResponse = GithubResponse(100,"testname",
            false,"testdescription",
            "10/05/2018",1,"testURL",
            10,5, "swift",
            Owner("testURL","me"),"testheadline",
            "10/5/2018"
    )


    fun setupEnvironment() {
        mContext = InstrumentationRegistry.getTargetContext()
        mApp = InstrumentationRegistry.getTargetContext().applicationContext as TestApplication
        mUtilModule = UtilModule(mContext)
    }

    protected fun setupLoopers() {
        //to make sure subscribeOn and observeOn run on same thread
        //async call becomes synchronous, thus waits for response
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler -> Schedulers.trampoline() }
        RxJavaPlugins.setIoSchedulerHandler { scheduler -> Schedulers.trampoline() }
    }

    protected fun tearDownLoopers() {
        RxAndroidPlugins.reset()
        RxJavaPlugins.reset()
    }

}