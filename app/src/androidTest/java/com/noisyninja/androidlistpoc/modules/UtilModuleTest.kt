package com.noisyninja.androidlistpoc.modules

import android.support.test.runner.AndroidJUnit4
import com.noisyninja.androidlistpoc.R
import com.noisyninja.androidlistpoc.model.GithubResponse
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by sudiptadutta on 23/05/18.
 */

@RunWith(AndroidJUnit4::class)
class UtilModuleTest : Base() {

    @Before
    fun setup() {
        setupEnvironment()
    }

    @After
    fun teardown() {
    }

    @Test
    fun testStringRes() {
        Assert.assertNotNull(mUtilModule.getStringRes(R.string.app_name))
    }

    @Test
    fun testStringPref() {
        val key = GithubResponse::javaClass.name
        val value = githubResponse.toString()
        mUtilModule.setStringPref(key, value)
        Assert.assertNotNull(mUtilModule.getStringPref(key))
        Assert.assertEquals(mUtilModule.getStringPref(key), value)
        mUtilModule.deleteStringPref(key)
        Assert.assertNull(mUtilModule.getStringPref(key))
    }

}
