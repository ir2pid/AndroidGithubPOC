package com.noisyninja.androidlistpoc.modules

import android.support.test.runner.AndroidJUnit4
import com.noisyninja.androidlistpoc.BuildConfig
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by sudiptadutta on 23/05/18.
 */

@RunWith(AndroidJUnit4::class)
class NetworkModuleTest : BaseRepository() {

    @Before
    fun setup() {
        setupEnvironment()
        setupLoopers()
    }

    @After
    fun teardown() {
        tearDownLoopers()
    }

    @Test
    fun serverCallWithSuccess() {
        val url = BuildConfig.BASE_URL
        setupServer(url)

        val networkObservable = mNetworkModule.getRepos()
        networkObservable.subscribe(mSubscriber)

        mSubscriber.assertNoErrors()

        mSubscriber.assertValue({ it ->
            it.isNotEmpty()
        })

        mSubscriber.assertValue({ it ->
            it[0].name?.isNotEmpty()!! &&
                    it[0].id != 0 &&
                    it[0].html_url?.isNotEmpty()!!
        })
    }

    /**
     * test for network availability
     */
    @Test
    fun serverCallWithError() {
        //this is an invalid uri
        val url = BuildConfig.BASE_URL + "doestNotExist/"
        setupServer(url)

        val networkObservable =
                mNetworkModule.getRepos()

        networkObservable.subscribe(mSubscriber)
        mSubscriber.assertError({ t: Throwable -> t.message!!.isNotEmpty() })

    }
}
