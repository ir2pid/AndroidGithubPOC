package com.noisyninja.androidlistpoc.modules

import android.support.test.runner.AndroidJUnit4
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by sudiptadutta on 23/05/18.
 */

@RunWith(AndroidJUnit4::class)
class DatabaseModuleTest : BaseRepository() {

    @Before
    fun setup() {
        setupEnvironment()
        setupDatabase()
    }

    @After
    fun teardown() {
        teardownDatabase()
    }

    @Test
    fun testInsert() {
        mIDatabase.databaseDao().insertRepo(githubResponse)

        val meList = mIDatabase.databaseDao().allRepos.getValueBlocking()
        Assert.assertEquals(meList?.size, 1)
    }

    @Test
    fun testDelete() {
        mIDatabase.databaseDao().insertRepo(githubResponse)

        var vehicleList = mIDatabase.databaseDao().allRepos.getValueBlocking()

        Assert.assertEquals(vehicleList?.size, 1)

        val retrievedMe = vehicleList?.get(0)
        mIDatabase.databaseDao().deleteRepo(retrievedMe)

        vehicleList = mIDatabase.databaseDao().allRepos.getValueBlocking()
        Assert.assertEquals(vehicleList?.size, 0)
    }
}
