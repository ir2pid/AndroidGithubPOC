# AndroidGithubPOC
[![Build Status](https://travis-ci.org/ir2pid/AndroidGithubPOC.svg?branch=master)](https://travis-ci.org/ir2pid/AndroidGithubPOC)
[![codebeat badge](https://codebeat.co/badges/1d100d96-7f5b-43fb-82ef-ede4ea0172c8)](https://codebeat.co/projects/github-com-ir2pid-androidgithubpoc-master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/694686265c674857805ae9ef4269a6cf)](https://www.codacy.com/app/ir2pid/AndroidGithubPOC?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=ir2pid/AndroidGithubPOC&amp;utm_campaign=Badge_Grade)
[![codecov](https://codecov.io/gh/ir2pid/AndroidGithubPOC/branch/master/graph/badge.svg)](https://codecov.io/gh/ir2pid/AndroidGithubPOC)
[![GitHub last commit](https://img.shields.io/github/last-commit/ir2pid/AndroidGithubPOC.svg)](https://github.com/ir2pid/AndroidGithubPOC)
</br><!--
[![shields](https://img.shields.io/badge/minSdkVersion-15-yellowgreen.svg)](https://github.com/ir2pid/AndroidArch)
[![shields](https://img.shields.io/badge/targetSdkVersion-26-orange.svg)](https://github.com/ir2pid/AndroidArch)
[![License Apache 2.0](https://img.shields.io/badge/License-Apache%202.0-blue.svg?style=true)](https://github.com/ir2pid/AndroidArch)
-->
</br>

</br>

A proof of concept for google's architecture components which uses Github api 
to fetch repo data and display in a master detail view

coded in a mixture of kotlin and Java uses android architecture components like
- Uses MVVM architecture with data binding
- LiveData for observable data source,
- ViewModel for Activity/Fragment lifecycle awareness
- Room for ORM
- okHttp and Retrofit for network calls
- RxJava for observing network data and applying filters on stream
- Dagger2 for dependency injection

### For unit testing, integration and UI testing.
- Junit,
- Mockito and
- Espresso

### For CI and static code analysis 
- Travis
- CodeBeat
- Codacy
- Codecov